#include "wstp.h"
#include <emacs-module.h>
#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std::string_literals;
using namespace std;
namespace emacs {
class Value {
  emacs_env* env;
  emacs_value e;

public:
  Value(emacs_env* env, emacs_value e) : env(env), e(e){};
  Value(emacs_env* env, int e) : env(env), e(env->make_integer(env, e)){};
  Value(emacs_env* env, double e) : env(env), e(env->make_float(env, e)){};
  Value(emacs_env* env, std::string e)
      : env(env), e(env->make_string(env, e.c_str(), e.length())){};
  explicit operator int() { return env->extract_integer(env, e); }
  explicit operator float() { return env->extract_float(env, e); }
  explicit operator std::string() {
    long type_name_length{};
    env->copy_string_contents(env, e, nullptr, &type_name_length);
    string symbol_name(type_name_length - 1, ' ');
    env->copy_string_contents(env, e, symbol_name.data(), &type_name_length);
    return symbol_name;
  }
  operator emacs_value() { return e; }
};

class Instance {
  emacs_env* env;

public:
  Instance(emacs_env* env) : env(env){};
  emacs_env* getEnv() { return env; };

  template <typename... Args> Value funcall(string function, Args&&... a) {
    return {env, env->funcall(env, env->intern(env, function.c_str()), long(sizeof...(a)),
                              (emacs_value[]){a...})};
  }

  Value funcall(string function, vector<emacs_value> argsv) {
    emacs_value args[argsv.size()];
    for(size_t i = 0; i < argsv.size(); i++)
      args[i] = argsv[i];
    return {env, env->funcall(env, env->intern(env, function.c_str()), argsv.size(), args)};
  }

  string getSymbolName(emacs_value symbol) { return (string)funcall("symbol-name", symbol); }
  string getTypeName(emacs_value value) { return getSymbolName(funcall("type-of", value)); }
  string getTypeName(Value value) { return getSymbolName(funcall("type-of", value)); }
};
} // namespace emacs

namespace WSTP {
class Link {
public:
  Link(){};
  WSENV ep{};
  WSLINK lp{};
};
} // namespace WSTP

namespace translator {
void putSExprOnLink(emacs::Instance& instance, emacs_value const& expression, WSTP::Link& link) {
  auto env = instance.getEnv();

  std::unordered_map<string, std::function<void(emacs_value, int const*, int const*)>>
      eLispToWSTPtranslator{
          {"string",
           [&](auto arg, auto, auto) {
             WSPutString(link.lp, ((string)emacs::Value(env, arg)).c_str());
           }}, //
          {"integer",
           [&](auto arg, auto, auto) { WSPutInteger(link.lp, (int)emacs::Value(env, arg)); }}, //
          {"float",
           [&](auto arg, auto, auto) { WSPutFloat(link.lp, (float)emacs::Value(env, arg)); }}, //
          {"symbol",
           [&](auto arg, auto argumentNumber, auto numberOfArguments) {
             auto symbolNameString = instance.getSymbolName(arg);
             if(argumentNumber && *argumentNumber == 0)
               WSPutFunction(link.lp, symbolNameString.c_str(), *numberOfArguments);
             else
               WSPutSymbol(link.lp, symbolNameString.c_str());
           }}, //
          {"cons",
           [&](auto arg, auto, auto) {
             auto sExprLength = (int)instance.funcall("length", arg);
             for(int i = 0; i < sExprLength; i++) {
               auto element = instance.funcall("nth", emacs::Value(env, i), arg);
               eLispToWSTPtranslator.at(instance.getTypeName(element))(element, &i,
                                                                       (int[]){sExprLength - 1});
             }
           }} //
      };

  eLispToWSTPtranslator.at(instance.getTypeName(expression))(expression, nullptr, nullptr);
  WSEndPacket(link.lp);
}
} // namespace translator

extern "C" {
int plugin_is_GPL_compatible;

int emacs_module_init(struct emacs_runtime* ert) {
  if(ert->size < sizeof(*ert))
    return 1;

  emacs_env* env = ert->get_environment(ert);
  env->funcall(env, env->intern(env, "defalias"), 2,
               (emacs_value[]){env->intern(env, "wstp-init"),
                               env->make_function(
                                   env, 0, emacs_variadic_function,
                                   [](emacs_env * env, ptrdiff_t nargs, emacs_value * args,
                                      void* data) noexcept {
                                     auto link = new WSTP::Link;
                                     int err;
                                     vector<string> arguments{};
                                     for(size_t i = 0; i < nargs; i++)
                                       arguments.emplace_back(emacs::Value(env, args[i]));
                                     char* argv[nargs];
                                     for(size_t i = 0; i < nargs; i++)
                                       argv[i] = arguments.at(i).data();

                                     link->ep = WSInitialize((WSParametersPointer)0);
                                     if(link->ep == (WSENV)0)
                                       exit(1);
                                     link->lp = WSOpenArgcArgv(link->ep, nargs, argv, &err);
                                     if(link->lp == (WSLINK)0)
                                       exit(2);

                                     return env->make_user_ptr(
                                         env, [](void* untyped_link) noexcept {
                                           auto link = static_cast<WSTP::Link*>(untyped_link);
                                           cerr << "closing link" << endl;
                                           WSClose(link->lp);
                                           WSDeinitialize(link->ep);
                                           delete link;
                                         },
                                         link);
                                   },
                                   "create the wstp connection", nullptr)});

  env->funcall( //
      env, env->intern(env, "defalias"), 2,
      (emacs_value[]){
          env->intern(env, "wstp-end"),
          env->make_function(
              env, 1, 1,
              [](emacs_env * env, ptrdiff_t nargs, emacs_value * args, void* data) noexcept {
                WSPutFunction(static_cast<WSTP::Link*>(env->get_user_ptr(env, args[0]))->lp, "Exit",
                              0);
                return (emacs_value)emacs::Value(env, "okay");
              },
              "end the wstp connection", nullptr)});

  env->funcall(
      env, env->intern(env, "defalias"), 2,
      (emacs_value[]){
          env->intern(env, "wstp-run"),
          env->make_function(
              env, 2, 2,
              [](emacs_env * env, ptrdiff_t nargs, emacs_value * args, void* data) noexcept {
                auto instance = emacs::Instance(env);
                auto link = static_cast<WSTP::Link*>(env->get_user_ptr(env, args[0]));
                translator::putSExprOnLink(instance, args[1], *link);

                int pkt;
                while((pkt = WSNextPacket(link->lp), pkt) && pkt != RETURNPKT)
                  WSNewPacket(link->lp);
                unordered_map<char, function<emacs_value()>> mappers{
                    {WSTKFUNC,
                     [&] {
                       char const* name;
                       int args;
                       WSGetFunction(link->lp, &name, &args);
                       if(!name)
                         instance.funcall(
                             "error",
                             emacs::Value{env, "function packet with null name received, " +
                                                   to_string(args) + " arguments"});
                       vector<emacs_value> argsv;
                       if("List"s != name)
                         argsv.push_back({env->intern(env, name)});
                       for(size_t i = 0; i < args; i++)
                         argsv.push_back(mappers.at(WSGetType(link->lp))());
                       auto result = instance.funcall("list", argsv);
                       WSReleaseSymbol(link->lp, name);
                       return result;
                     }},
                    {WSTKINT,
                     [&] {
                       int i;
                       WSGetInteger(link->lp, &i);
                       return emacs::Value(env, i);
                     }},
                    {WSTKREAL,
                     [&] {
                       double d;
                       WSGetReal(link->lp, &d);
                       return emacs::Value(env, d);
                     }},
                    {WSTKSYM,
                     [&] {
                       const char* q;
                       WSGetSymbol(link->lp, &q);
                       auto result = emacs::Value(env, env->intern(env, q));
                       WSReleaseString(link->lp, q);
                       return result;
                     }},
                    {WSTKERROR,
                     [&] {
                       auto result =
                           instance.funcall("error", emacs::Value{env, WSErrorMessage(link->lp)});
                       WSClearError(link->lp);
                       return result;
                     }},
                    {WSTKSTR, [&] {
                       const char* q;
                       WSGetString(link->lp, &q);
                       auto result = emacs::Value(env, q);
                       WSReleaseString(link->lp, q);
                       return result;
                     }}}; //
                return mappers.at(WSGetType(link->lp))();
              },
              "run the wstp program", nullptr)});
  return 0;
}
}
